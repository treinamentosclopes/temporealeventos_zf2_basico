<?php
use Zend\Loader\StandardAutoloader;
use Zend\EventManager\EventManager;

require 'vendor/Zend Framework 2/Zend/Loader/StandardAutoloader.php';

$autoloader = new StandardAutoloader(array(
	'autoregister_zf' => TRUE
));
$autoloader->register();


spl_autoload_register(function($class){
    $file = $class . '.php';
    if(file_exists($file))
	   require $file;
});

$predio = new Predio2();

$eventManager = new EventManager();
$eventManager->attach('INCENDIO',array(
    new Bombeiro2(), 'apagarIncendio'
));

$predio->setEventManager($eventManager);
$predio->setTemperatura(50);