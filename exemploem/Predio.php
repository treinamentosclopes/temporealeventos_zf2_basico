<?php
// Inteface para implementacao para criar Observadores dentro do projeto
class Predio implements \SplSubject
{
    private $observers = array();
    private $temperatura = 0;
    private $notified = false;

    // excluir o observador padrao da implementacao
    public function detach(SplObserver $observer)
    {
    	if(in_array($this->observers, $observer))
    	{
    		foreach ($this->observers as $index => $observerStored)
    		{
    			if($observer === $observerStored)
    			{
    			    unset($this->observers[$index]);
    			    break;
    			}
    		}
    	}
    }

    // inseri o observador padrao da implementacao
    public function attach(SplObserver $observer)
    {
    	$this->observers[] = $observer;
    }

    // metodo que chama os observadores padrao da implementacao
    public function notify()
    {
    	foreach ($this->observers as $observer)
    	{
    		$observer->update($this);
    	}    	
    }
    
    public function setTemperatura($temperatura)
    {
    	$this->temperatura = $temperatura;
    	
    	if($this->temperatura > 40 && !$this->notified)
    	{
    	    $this->notified = true;
    	    $this->notify();
    	    $this->notified = false;
    	}
    }
    
    public function getTemperatura()
    {
    	return $this->temperatura;
    }
}
?>