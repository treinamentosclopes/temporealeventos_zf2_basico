<?php

class Bombeiro implements \SplObserver
{

    public function update(SplSubject $subject)
    {
    	while(($temperatura = $subject->getTemperatura()) > 40)
    	{
    		$subject->setTemperatura($temperatura - 1);
    		echo 'Baixando temperatura para ' . $subject->getTemperatura() . '<br>';
    	}
    }
}

?>