<?php
use Zend\EventManager\EventManager;

// Inteface para implementacao para criar Observadores dentro do projeto
class Predio2
{
    private $temperatura = 0;
    private $notified = false;
    /**
     *
     * @var EventManager
     */
    private $eventManager;
    
    public function setEventManager(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }
    
    public function getEventManager()
    {
        return $this->eventManager;
    }
    
    public function setTemperatura($temperatura)
    {
    	$this->temperatura = $temperatura;
    	
    	if($this->temperatura > 40 && !$this->notified)
    	{
    	    $this->notified = true;
    	    $this->eventManager->trigger('INCENDIO',$this);
    	    $this->notified = false;
    	}
    }
    
    public function getTemperatura()
    {
    	return $this->temperatura;
    }
}
?>