<?php

use Zend\EventManager\Event;

class Bombeiro2
{
    
    public function apagarIncendio(Event $event)
    {
        $subject = $event->getTarget(); // esse metodo diz quem disparou o evento
        
    	while(($temperatura = $subject->getTemperatura()) > 40)
    	{
    		$subject->setTemperatura($temperatura - 1);
    		echo 'Baixando temperatura para ' . $subject->getTemperatura() . '<br>';
    	}
    }
}

?>