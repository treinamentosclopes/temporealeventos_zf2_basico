<?php
use Zend\Di\Di;

/* esse trecho serve para usar qualquer componente do zf2 */
use Zend\Loader\StandardAutoloader;

require 'vendor/Zend Framework 2/Zend/Loader/StandardAutoloader.php';
$autoloader = new StandardAutoloader(array(
	'autoregister_zf' => TRUE
));
#$autoloader->registerNamespace('\\', __DIR__); // add diretorio raiz
$autoloader->register();
/* fim do trecho */

spl_autoload_register(function($class){
	require $class . '.php';
});


$di = new Di();

$dataSource = new \DataSource('qualquercoisa.dat');
$gatewayManager = new \GatewayManager($dataSource);
$powerActiverRecords = new \PowerActiveRecords($gatewayManager);
$megaPowerResultSet = new \MegaPowerResultSet($powerActiverRecords);
$supraHiperExtraController = new \SupraHiperExtraController($megaPowerResultSet);
$application = new \UltraApplication($supraHiperExtraController);

var_dump($application);



