<?php
use Zend\Di\Di;

/* esse trecho serve para usar qualquer componente do zf2 */
use Zend\Loader\StandardAutoloader;

require 'vendor/Zend Framework 2/Zend/Loader/StandardAutoloader.php';
$autoloader = new StandardAutoloader(array(
	'autoregister_zf' => TRUE
));
#$autoloader->registerNamespace('\\', __DIR__); // add diretorio raiz
$autoloader->register();
/* fim do trecho */

spl_autoload_register(function($class){
    $file = $class . '.php';
    if(file_exists($file))
        require $file;
});

// refexao de classes
$di = new Di();
$di->instanceManager()->setParameters('DataSource', array(
    'file' => 'qualquercoisa.dat'
));
$application = $di->get('UltraApplication');

var_dump($application);



