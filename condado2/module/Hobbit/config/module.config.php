<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Hobbit\Controller\Index' => 'Hobbit\Controller\IndexController',
            'Hobbit\Controller\Tocas' => 'Hobbit\Controller\TocasController',
            'Hobbit\Controller\Hobbit' => 'Hobbit\Controller\HobbitController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'hobbit' => array(
                'type'    => 'Segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/hobbit[/[:action[/:id]]]',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Hobbit\Controller',
                        'controller'    => 'Hobbit',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,                
            ),
            'tocas' => array(
                'type'    => 'Segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/tocas[/[:action[/:id]]]',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Hobbit\Controller',
                        'controller'    => 'Tocas',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,                
            ),
            'menu' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/menu',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Hobbit\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Hobbit' => __DIR__ . '/../view',
        ),
        'template_map' => array(
        	'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
    	   'batman' => 'Coiote\View\Helper\SimpleForm'
        )
    ),
    'controller_plugins' => array(
	    'invokables' => array(
    	   'getRecords' => 'Coiote\Controller\Plugin\GetRecords'
        )
    )
);
