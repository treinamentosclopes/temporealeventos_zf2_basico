<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Hobbit for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Hobbit\Controller;

use Coiote\Controller\ControllerAbstract;

class HobbitController extends ControllerAbstract
{
    protected $formClass = 'Hobbit\Form\Hobbit';
    
    protected $modelClass = 'Hobbit\Model\Hobbit';
    
    protected $module = 'hobbit';
        
    public function getTable()
    {
    	return $this->getServiceLocator()->get('HobbitTable');
    }
    
    public function getTables()
    {
        $tables = parent::getTables();
        $tables['TocasTable'] = $this->getServiceLocator()->get('TocasTable');        
    	return $tables;
    }
    
}