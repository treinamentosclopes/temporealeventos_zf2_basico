<?php
namespace Hobbit\Form;

use Zend\Form\Form;
use Zend\Form\Element\Text;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Select;
use Coiote\Form\IdInterface;

class Hobbit extends Form implements IdInterface
{

    public function __construct($name = 'hobbit')
    {
    	parent::__construct($name);
    	
    	$this->setAttribute('method', 'post');
    	
    	$elementOrFieldset = new Text('nome');
    	$elementOrFieldset->setLabel('Nome:');
    	$elementOrFieldset->setAttribute('autofocus', 'autofocus');
    	$this->add($elementOrFieldset);
    	
    	$elementOrFieldset = new Hidden('id');    	 
    	$this->add($elementOrFieldset);
    	
    	$elementOrFieldset = new Select('id_tocas');
    	$elementOrFieldset->setLabel('Tocas:');
    	$this->add($elementOrFieldset);
    	
    	$elementOrFieldset = new Submit('gravar');
    	$elementOrFieldset->setValue('gravar');
    	$this->add($elementOrFieldset);
    	    	
    }
    
    /**
     * $tables = array(
     *      'table1' => object of TableGatewayAbstract
     * )
     * @param integer $id
     * @param array $tables
     */
    public function setId($id, array $tables)
    {
        $tocasTable = $tables['TocasTable'];
        $tocas = $tocasTable->fecthAll();
        
        $options = array();
        foreach ($tocas as $toca)
        {
        	$options[$toca->id] = $toca->nome;
        }
        $this->get('id_tocas')->setValueOptions($options);        
        
    	if($id == null) return;
    	
    	$hobbitTable = $tables['HobbitTable'];
    	
    	$hobbit = $hobbitTable->fetchOne($id);
    	
    	if($hobbit != null)
    	{
    		$this->get('nome')->setValue($hobbit->nome);
    		$this->get('id')->setValue($id);
    		$this->get('id_tocas')->setValue($hobbit->tocas->id);
    	}    	
    }
    
    
    
}

?>