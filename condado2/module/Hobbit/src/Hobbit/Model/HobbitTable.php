<?php
namespace Hobbit\Model;

use Coiote\TableGateway\TableGatewayAbstract;
use Coiote\Model\ModelAbstract;

class HobbitTable extends TableGatewayAbstract
{   
    public function getSet(ModelAbstract $model)
    {
        $set = array(
            'nome'     => $model->nome,
            'id_tocas' => $model->tocas->id
        );
        return $set;
    }
}

?>