<?php
namespace Hobbit\Model;

use Coiote\TableGateway\TableGatewayAbstract;
use Coiote\Model\ModelAbstract;

class TocasTable extends TableGatewayAbstract
{
    public function getSet(ModelAbstract $model)
    {
        $set = array(
            'nome' => $model->nome
        );        
        return $set;
    }
}

?>