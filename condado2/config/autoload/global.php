<?php
use Zend\Log\Logger;
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

$streamOrUrl = realpath(__DIR__ . '/../../data/log') . '/debug.log';

return array(
    // ...
    'db' => array(
	    'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=condado;hostname=localhost',
        'username'       => 'root',
        'password'       => '',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        )
    ),
    'log' => array(
        'writers' => array(
            'writer1' => array(
                'name'     => 'Stream',
                'priority' => Logger::DEBUG,
                'options'  => array(
            	       'stream' => $streamOrUrl
                )
            )
        )
    ),
    'service_manager' => array(
	    'factories' => array(
    	    'Zend\Db\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
	        'logger'          => 'Zend\Log\LoggerServiceFactory'
        )
    )
);
