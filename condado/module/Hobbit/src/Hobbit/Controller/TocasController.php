<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Hobbit for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Hobbit\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Hobbit\Form\Tocas as TocasForm;
use Hobbit\Model\Tocas;
use Hobbit\Model\TocasTable;

class TocasController extends AbstractActionController
{
    public function indexAction()
    {
        $tocasTable = $this->getTocasTable();
        $records = $tocasTable->fecthAll();
        return array('records' => $records);
    }

    public function editAction()
    {
        $id = $this->params('id',null);
        
        if(isset($_SESSION['form']))
        {
        	$form = $_SESSION['form'];
        	unset($_SESSION['form']);
        } else {
            $form = new TocasForm();
        }
        
        $form->setAttribute('action', $this->
            url()->fromRoute('tocas',array(
                'action' => 'save'
        )));
        $form->setId($id, $this->getServiceLocator());
        
        return array('form' => $form);
    }
    
    public function saveAction()
    {
        // abaixo cria o objeto model
    	$tocas = new Tocas();
    	$tocas->exchangeArray((array)$this->getRequest()->getPost()); // o array colocado � pra for�ar o tipo � o que chamamos de cast
    	// abaixo cria o form e pega o inputFilter do obrjeto model
    	$form = new TocasForm();
    	$form->setInputFilter($tocas->getInputFilter());
    	$form->bind($tocas); // recebe os dados
    	if(!$form->isValid())
    	{
    		$_SESSION['form'] = $form;
    		return $this->redirect()->toRoute('tocas',array('action' => 'edit'));
    	}
    	
    	$tocasTable = $this->getTocasTable();
    	$tocasTable->save($tocas);
    	
    	$this->redirect()->toRoute('tocas');
    }
    
    public function deleteAction()
    {
        $id = $this->params('id',null);
        $tocasTable = $this->getTocasTable();
        $tocasTable->delete($id);
        
        $this->redirect()->toRoute('tocas');
    }
    
    // com a sinalizacao abaixo faz com que o ao chamar o metodo traga os metodos que o service locator retorna
    /**
     * 
     * @return TocasTable
     */
    private function getTocasTable()
    {
    	return $this->getServiceLocator()->get('TocasTable');
    }
    
}
