<?php
namespace Hobbit\Form;

use Zend\Form\Form;
use Zend\Form\Element\Text;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Submit;
use Zend\ServiceManager\ServiceManager;

class Tocas extends Form
{

    public function __construct($name = 'tocas')
    {
    	parent::__construct($name);
    	
    	$this->setAttribute('method', 'post');
    	
    	$elementOrFieldset = new Text('nome');
    	$elementOrFieldset->setLabel('Nome:');
    	$elementOrFieldset->setAttribute('autofocus', 'autofocus');
    	$this->add($elementOrFieldset);
    	
    	$elementOrFieldset = new Hidden('id');    	 
    	$this->add($elementOrFieldset);
    	
    	$elementOrFieldset = new Submit('gravar');
    	$elementOrFieldset->setValue('gravar');
    	$this->add($elementOrFieldset);
    	    	
    }
    
    public function setId($id, ServiceManager $sm)
    {
    	if($id == null) return;
    	
    	$tocasTable = $sm->get('TocasTable');
    	
    	$tocas = $tocasTable->fetchOne($id);
    	
    	if($tocas != null)
    	{
    		$this->get('nome')->setValue($tocas->nome);
    		$this->get('id')->setValue($id);
    	}
    	
    }
    
    
    
}

?>