<?php
namespace Hobbit\Model;

use Zend\InputFilter\InputFilter;
use Zend\Filter\FilterChain;
use Zend\InputFilter\Input;
use Zend\Filter\Int;
use Zend\Validator\ValidatorChain;
use Zend\I18n\Filter\Alnum;
use Zend\Validator\StringLength;
use Zend\Validator\Digits;
use Zend\Filter\StringTrim;
use Zend\ServiceManager\ServiceManager;

class Hobbit
{
    /**
     * 
     * @var integer
     */
    public $id = null;
    
    /**
     * 
     * @var string
     */
    public $nome = null;
    
    /**
     * 
     * @var Tocas
     */
    public $tocas = null;
    
    /**
     * 
     * @var InputFilter
     */
    private $inputFilter = null;
    
    /**
     * 
     * @var ServiceManager
     */
    private $serviceManager = null;
    
    
    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }
    
    public function exchangeArray(array $array)
    {
    	$this->id   = isset($array['id']) ? $array['id'] : null;
    	$this->nome = isset($array['nome']) ? $array['nome'] : null;
    	
    	$idTocas = isset($array['id_tocas']) ? $array['id_tocas'] : null;
    	
    	$tocasTable = $this->serviceManager->get('TocasTable');
    	$toca = $tocasTable->fetchOne($idTocas);
    	$this->tocas = $toca;
    	
    	if($this->tocas == null){
    	    $this->tocas = new Tocas();
    	}   
    	 	    
    	$this->tocas->id = $idTocas;
    }
    
    public function toArray()
    {        
    	$array = get_object_vars($this);
        $array['tocas'] = $this->tocas->nome;
        return $array;
    }
    
    public function getInputFilter()
    {
    	if($this->inputFilter == null)
    	{
    		$inputFilter = new InputFilter();
    		
    		$input = new Input('id');
    		$input->setAllowEmpty(TRUE); // diz se o valor id pode ou nao ser vazio
    		
    		$filterChain = new FilterChain(); // cadeias de filtros
    		$filterChain->attach(new Int());    		
    		$input->setFilterChain($filterChain);
    		
    		$validatorChain = new ValidatorChain();
    		//$validatorChain->addValidator(new \Zend\I18n\Validator\Int());
    		$validatorChain->addValidator(new Digits()); // para servidor xampp, nos demais funciona de tranquilo
    		$input->setValidatorChain($validatorChain);
    		
    		$inputFilter->add($input);
    		
    		// campo nome
    		$input = new Input('nome');
    		
    		$filterChain = new FilterChain(); // cadeias de filtros
    		//$filterChain->attach(new Alnum()); // Filter
    		$filterChain->attach(new StringTrim()); // para servidor xampp, nos demais funciona de tranquilo
    		$input->setFilterChain($filterChain);
    		
    		$validatorChain = new ValidatorChain();
    		$validatorChain->addValidator(new StringLength(array(
            	'min' => 3,
                'max' => 30
            )));    		
    		$input->setValidatorChain($validatorChain);    
    				
    		$inputFilter->add($input);
    		
    		// campo id da tabela Tocas
    		$input = new Input('id_tocas');
    		
    		$filterChain = new FilterChain(); // cadeias de filtros
    		$filterChain->attach(new Int());    		
    		$input->setFilterChain($filterChain);
    		
    		$validatorChain = new ValidatorChain();
    		$validatorChain->addValidator(new Digits()); // para servidor xampp, nos demais funciona de tranquilo
    		$input->setValidatorChain($validatorChain);
    		
    		$inputFilter->add($input);
    		
    		
    		$this->inputFilter = $inputFilter;
    	}
    	
    	return $this->inputFilter;
    }
    
    public function getArrayCopy()
    {
        $array = get_object_vars($this);
        $array['id_tocas'] = $this->tocas->id;
        return $array;
    }
    
    
    
    
}

?>