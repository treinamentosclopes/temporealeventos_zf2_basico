<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use Zend\Validator\AbstractValidator;
use Zend\Mvc\I18n\Translator as MvcTranslator;
use Zend\I18n\Translator\Translator;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        // exemplo de translator em tempo real mas tbm pode ser definido diretamente no arquivo
        // modulo.config.php pega do arquivo .mo
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $translator->setLocale('pt_BR');
        
        // traduzindo as mensagem de validacao
        /*$validatorTranslator = new Translator();        
        $filename = realpath(__DIR__) . '/../../vendor/zendeframework/zendframework/resources/languages/pt_BR/Zend_Validate.php';
        echo $filename;exit;
        $validatorTranslator->addTranslationFile('phpArray', $filename);
        
        $mvcTranslator = new MvcTranslator($validatorTranslator);
        
        AbstractValidator::setDefaultTranslator($mvcTranslator);*/
        
        // instancia a sessao
        $sessionManager = new SessionManager();
        $sessionManager->start();
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
