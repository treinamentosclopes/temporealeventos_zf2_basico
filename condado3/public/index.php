<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';
require 'init_myautoloader.php';

Coiote\Service\Authentication::setController('Teste');
Coiote\Service\Authentication::setAction('teste');

// Run the application! // Configura��o para usar sem virtual host necessaria
$application = Zend\Mvc\Application::init(require 'config/application.config.php');
$application->getRequest()->setBaseUrl('/condado3');
$application->run();
