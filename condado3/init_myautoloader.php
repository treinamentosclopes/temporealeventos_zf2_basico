<?php

$coiotePath = realpath(__DIR__ . '/vendor/acme/coiote/library/Coiote');
#echo $coiotePath; exit;

Zend\Loader\AutoloaderFactory::factory(array(
    'Zend\Loader\StandardAutoloader' => array(
        'autoregister_zf' => true,
        // aqui cria o array do namespaces igual do modulo
        'namespaces' => array(                    
            'Coiote' => $coiotePath
        ),
    )
));