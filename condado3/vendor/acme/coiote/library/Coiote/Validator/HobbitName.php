<?php
namespace Coiote\Validator;

use Zend\Validator\AbstractValidator;

class HobbitName extends AbstractValidator
{

    public function isValid($value)
    {
    	if(stripos($value, 'a')!== FALSE && stripos($value, 'e')!== FALSE && stripos($value, 'r')!== FALSE)
    	{
    		return TRUE;
    	}
    	
    	$this->abstractOptions['messages'] = array(
    		'This is not a hobbit name'
    	);
    	return FALSE;
    }
}

?>