<?php
namespace Coiote\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Form\Form;

class SimpleForm extends AbstractHelper
{
    public function render(Form $form)
    {        
        $html = '';               
    	$html .= $this->getView()->form()->openTag($form);// o metodo getView() me da total referencia da view
    	$html .= $this->getView()->formCollection($form);
    	$html .= $this->getView()->form()->closetag();    	
    	return $html;
    } 
}

?>