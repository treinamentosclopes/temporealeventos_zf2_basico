<?php
namespace Coiote\TableGateway;

use Zend\Db\TableGateway\TableGateway;
use Coiote\Model\ModelAbstract;
use Zend\ServiceManager\ServiceManager;

abstract class TableGatewayAbstract
{
    /**
     *
     * @var TableGateway
     */
    protected $tableGateway = null;
    
    /**
     * 
     * @var ServiceManager
     */
    protected $serviceManager = null;
    
    public function __construct(TableGateway $tableGateway, ServiceManager $serviceManager)
    {
        $this->tableGateway   = $tableGateway;
        $this->serviceManager = $serviceManager;
    }
    
    abstract function getSet(ModelAbstract $model); // Obriga a classe filha a implementar esse method
    
    public function save(ModelAbstract $model)
    {
        // Recuperando o log da acao devemos guardar o retorno do insert.
        // Em seguida devemos recuperar o o logger atraves do service manager e chamamos o nivel que queremos saber a informacao
        
        $logger = $this->serviceManager->get('logger');
        $set = $this->getSet($model); // methoda da classe filha
        if($model->id == null)
        {            
            $result = $this->tableGateway->insert($set);            
            $logger->debug("Foram incluídos $result registros");
        }
        else {
            $where = array('id' => $model->id);
            $result = $this->tableGateway->update($set,$where);
            $logger->debug("Foram alterados $result registros");
        }         
    }
    
    public function fecthAll($where = null)
    {
        // debug
        $logger = $this->serviceManager->get('logger');        
        $resultSet = $this->tableGateway->select($where);
        // debug
        $sql = $this->tableGateway->getSql()->select()->getSqlString();
        $logger->debug($sql);
        
        return $resultSet;
    }
    
    public function fetchOne($id)
    {
        $where = array('id' => $id);
        $resultSet = $this->fecthAll($where);
        if($resultSet->count() == 1)
        {
            return $resultSet->current();
        }
        return null;
    }
    
    public function delete($id)
    {
        $where = array('id' => $id);
        $this->tableGateway->delete($where);
    }
}

?>