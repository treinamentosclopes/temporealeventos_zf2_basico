<?php
namespace Coiote\Model;

use Zend\Filter\FilterChain;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Validator\ValidatorChain;
use Zend\Validator\Digits;

abstract class ModelAbstract
{    
    /**
     * 
     * @var InputFilter
     */
    protected $inputFilter = null;
    
    /**
     * 
     * @var array
     */
    protected $tables = array();
    
    /**
     * 
     * @var array
     */
    protected $attributes = array();
    
    
    public function __construct(array $tables = null)
    {
        $this->tables = $tables;
    }
    
    public function exchangeArray(array $array)
    {
        foreach ($array as $attribute => $value)
        {
        	$this->$attribute = $value;
        }
    }
    
    public function toArray()
    {        
    	return get_object_vars($this);
    }
    
    public function getInputFilter()
    {
    	if($this->inputFilter == null)
    	{
    		$inputFilter = new InputFilter();
    		
    		foreach ($this->attributes as $attribute => $value)
    		{
    		    $input = new Input($attribute);
    		    $input->setAllowEmpty($value['allowEmpty']);
    		    
    		    $filterChain = new FilterChain();
    		    foreach ($value['filters'] as $filter)
    		    {
    		        $filterChain->attach($filter);
    		    }    		    
    		    $input->setFilterChain($filterChain);
    		    
    		    $validatorChain = new ValidatorChain();
    		    foreach ($value['validators'] as $validator)
    		    {
    		        $validatorChain->attach($validator);
    		    }
    		    $input->setValidatorChain($validatorChain);
    		    
    		    $inputFilter->add($input);
    		}
    		    		    	
    		$this->inputFilter = $inputFilter;
    	}
    	
    	return $this->inputFilter;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    // tira as referencias e instancias dos Tables
    public function clearTables()
    {
    	$this->tables = array();
    }
    
    public function getClearModel()
    {
    	$this->clearTables();
    	return $this;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
?>