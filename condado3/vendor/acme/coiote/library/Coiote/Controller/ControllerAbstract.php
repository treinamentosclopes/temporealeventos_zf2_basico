<?php
namespace Coiote\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Coiote\TableGateway\TableGatewayAbstract;

abstract class ControllerAbstract extends AbstractActionController
{
    /**
     * 
     * @var string
     */
    protected $module = NULL;
        
    /**
     * 
     * @var string
     */
    protected $formClass = NULL;
    
    /**
     * 
     * @var string
     */
    protected $modelClass = NULL;
    
    public function indexAction()
    {
        /*$table = $this->getTable();
        $records = $table->fecthAll();
        return array('records' => $records);*/
        return $this->getRecords();
    }
    
    public function editAction()
    {
        $id = $this->params('id',null);
    
        if(isset($_SESSION['form']))
        {
            $form = $_SESSION['form'];
            unset($_SESSION['form']);
        } 
        else 
        {
            $formClass = $this->formClass;
            $form = new $formClass();
        }
    
        $form->setAttribute('action', $this->
            url()->fromRoute($this->module, array(
                'action' => 'save'
            )));
        $form->setId($id, $this->getTables());
    
        return array('form' => $form);
    }
    
    public function saveAction()
    {
        $modelClass = $this->modelClass;
        $model = new $modelClass($this->getTables());
        $model->exchangeArray((array)$this->getRequest()->getPost());
        
        $formClass = $this->formClass;
        $form = new $formClass();
        $form->setId($model->id, $this->getTables());
        $form->setInputFilter($model->getInputFilter());
        $form->bind($model->getClearModel());

        if(!$form->isValid())
        {
            $_SESSION['form'] = $form;
            return $this->redirect()->toRoute($this->module, array('action' => 'edit'));
        }
         
        $table = $this->getTable();
        $table->save($model);
         
        $this->redirect()->toRoute($this->module);
    }
    
    public function deleteAction()
    {
        $id = $this->params('id',null);
        $table = $this->getTable();
        $table->delete($id);
    
        $this->redirect()->toRoute($this->module);
    }
        
    /**
     * @return TableGatewayAbstract
     */
    abstract public function getTable();
    
    /**
     * @return array
     */
    public function getTables()
    {
        $table = $this->getTable();
        
        $tokens = explode('\\', $this->modelClass);
        
        $tableName = end($tokens) . 'Table';
        
        $tables =  array(
            $tableName  => $table
        );
        return $tables;
    }
    
}