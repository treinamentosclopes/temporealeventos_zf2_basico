<?php
namespace Coiote\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class GetRecords extends AbstractPlugin
{
    // invoca o metodo
    public function __invoke()
    {
    	return $this->getRecords();
    }
    
    public function getRecords()
    {
        $table = $this->getController()->getTable();
        $records = $table->fecthAll();
        return array('records' => $records);
    }
}