<?php
namespace Coiote\Form;

interface IdInterface
{
    public function setId($id, array $tables);
}