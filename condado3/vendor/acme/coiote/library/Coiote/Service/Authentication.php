<?php
namespace Coiote\Service;

use Zend\EventManager\Event;
use Zend\Authentication\AuthenticationService;

// method's statics para deixar essa classe dinamica
class Authentication
{
    private static $controller = 'Index';
    private static $action = 'index';
    
    static function setController($controller)
    {
    	self::$controller = $controller;
    }
    
    static function setAction($action)
    {
        self::$action = $action;
    }
    
    public function checkIndentity(Event $event)
    {
    	$application = $event->getTarget();
    	
    	$url = $application->getRequest()->getRequestUri();
    	
    	$tokens = explode('/', $url);    	
    	$token = end($tokens);
    	
    	if($token == 'application' || $token == 'login')
    	{    	   
    		return;
    	}
    	
    	$authenticator = new AuthenticationService();
    	if(!$authenticator->hasIdentity())
    	{
    	    $router = $application->getServiceManager()->get('Router');// Recupera-se o route do zf2 para remontar o url com argumentos
    	    $url = $router->assemble(
    	        array(
    	    	   'controller' => 'Index',
    	           'action'     => 'index' 
    	        ),
    	        array(
    	    	   'name' => 'application'
    	        )
    	    );
    	    
    	    $response = $application->getResponse();
    	    $response->setHeaders(
    	        $response->getHeaders()->addHeaderLine('Location', $url)
            );
    	    $response->setStatusCode(302);
    	    $response->sendHeaders();
    	    exit;
    	}
    	
    }
}

?>