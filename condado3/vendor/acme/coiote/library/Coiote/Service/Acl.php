<?php
namespace Coiote\Service;

use Zend\EventManager\Event;
use Zend\Authentication\AuthenticationService;

// method's statics para deixar essa classe dinamica
class Acl
{
    
    public function checkPermissions(Event $event)
    {
        $authenticator = new AuthenticationService();
        if(!$authenticator->hasIdentity()) return;        
        
    	$application = $event->getTarget();
    	
    	$url = $application->getRequest()->getRequestUri();    	
    	$baseUrl = $application->getRequest()->getBaseUrl();
    	$url = str_replace($baseUrl, '', $url);
    	
    	$tokens = explode('/', $url);
    	if(end($tokens) == 'menu') return;
    	
    	$acl = $authenticator->getStorage()->read()->acl; // retorna um usuario
    	$roles = $acl->getRoles();
    	$role = current($roles);
    	
    	// cria execao caso esquecamos de definir algo no banco
    	try {
    		$hasPermission = $acl->isAllowed($role, $url);
    	} catch (Exception $e) {
    	    $hasPermission = false;
    	}
    	
    	if(!$hasPermission)
    	{
    	    $router = $application->getServiceManager()->get('Router');// Recupera-se o route do zf2 para remontar o url com argumentos
    	    $url = $router->assemble(
    	        array(
    	    	   'controller' => 'Hobbit',
    	           'action'     => 'index' 
    	        ),
    	        array(
    	    	   'name' => 'menu' //nome da rota
    	        )
    	    );
    	    
    	    $response = $application->getResponse();
    	    $response->setHeaders(
    	        $response->getHeaders()->addHeaderLine('Location', $url)
            );
    	    $response->setStatusCode(302);
    	    $response->sendHeaders();
    	    exit;
    	}
    	
    }
}

?>