<?php
namespace Application\Model;

use Zend\Permissions\Acl\Acl;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class Usuario
{
    public $usuario = NULL;
    public $acl;
    
    public function __construct($usuario, Adapter $zendDb)
    {
    	$this->usuario = $usuario;
    	
    	$this->acl = new Acl();
    	
    	// Papeis
    	/**
            SELECT * FROM papeis
            INNER JOIN papeis_usuario
            ON papeis.id = papeis_usuario.id_papel
            INNER JOIN usuarios
            ON usuarios.id = papeis_usuario.id_usuario
            WHERE usuarios.usuario = $usuario
    	 */
    	$select = new Select('papeis');
    	$select->columns(array('papel'))
    	       ->join('papeis_usuario', 'papeis.id = papeis_usuario.id_papel')
    	       ->join('usuarios', 'usuarios.id = papeis_usuario.id_usuario')
    	       ->where(array('usuarios.usuario' => $usuario));
    	
    	$statement = $zendDb->query($select->getSqlString(
    		$zendDb->getPlatform()
    	));
    	$resultSet = $statement->execute();
    	
    	
    	foreach($resultSet as $row)
    	{
    		$this->acl->addRole($row['papel']);
    	}
    	
    	// Recuros
    	/**
    	 SELECT * FROM recursos
    	 */
    	$select = new Select('recursos');
    	 
    	$statement = $zendDb->query($select->getSqlString(
    		$zendDb->getPlatform()
    	));
    	$resultSet = $statement->execute();
    	 
    	foreach($resultSet as $row)
    	{
    	    $this->acl->addResource($row['recurso']);
    	}
    	
    	// Recursos X papel
    	/**
    	 SELECT recurso FROM recursos
    	 INNER JOIN recursos_papel
    	 ON recursos.id = recursos_papel.id_recurso
    	 INNER JOIN papeis_usuario
    	 ON papeis_usuario.id_papel = recursos_papel.id_papel
    	 WHERE papeis_usuario.id_usuario = 
    	 (SELECT id FROM usuarios WHERE usuario = $usuario)
    	 */
    	$subSelect = new Select('usuarios');
    	$subSelect->columns(array('id'))
    	          ->where(array('usuario' => $usuario));
    	$subSelect = $subSelect->getSqlString(
    	   $zendDb->getPlatform()
    	);
    	
    	$select = new Select('recursos');
    	$select->columns(array('recurso'))
            	->join('recursos_papel', 'recursos.id = recursos_papel.id_recurso')
            	->join('papeis_usuario', 'papeis_usuario.id_papel = recursos_papel.id_papel');
    	 
        $sql =  $select->getSqlString($zendDb->getPlatform());
        $sql .= " WHERE papeis_usuario.id_usuario = ($subSelect)";
            	            	
    	$statement = $zendDb->query($sql);
    	$resultSet = $statement->execute();
    	 
    	foreach($resultSet as $row)
    	{
    	    foreach($this->acl->getRoles() as $role)
    	    {
    	        $this->acl->allow($role, $row['recurso']);
    	    }    	    
    	}    	    
    }
}

?>