<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable;
use Zend\Validator\NotEmpty;
use Application\Model\Usuario;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $messages = $this->flashMessenger()->getErrorMessages();
        $this->flashMessenger()->clearMessages();
        return new ViewModel(array(
        	'messages' => $messages
        ));
    }
    
    public function loginAction()
    {
    	$usuario = $this->getRequest()->getPost('usuario');
    	$senha = $this->getRequest()->getPost('senha');
    	
    	
    	// usando validadores isolados
    	$validator = new NotEmpty();
    	if(!$validator->isValid($usuario) || !$validator->isValid($senha))
    	{
    	    foreach($validator->getMessages() as $message)
    	    {
    	       $this->flashMessenger()->addErrorMessage($message);
    	    }
    	    return $this->redirect()->toRoute('application');
    	}
    	
    	$authenticator = new AuthenticationService();
    	
    	$zendDb = $this->getServiceLocator()->get('Zend\Db\Adapter');
    	$authAdapter = new DbTable($zendDb);
    	$authAdapter->setTableName('usuarios');
    	$authAdapter->setIdentityColumn('usuario');
    	$authAdapter->setCredentialColumn('senha');
    	$authAdapter->setIdentity($usuario);
    	$authAdapter->setCredential(md5($senha));
    	
    	$authenticator->setAdapter($authAdapter);
    	
    	$result = $authenticator->authenticate();
    	if($result->isValid())
    	{
    	    $authenticator->getStorage()->write(new Usuario($usuario, $zendDb));
    		$this->redirect()->toRoute('menu');
    	}
    	else 
    	{
    	    $messages = $result->getMessages();
    	    foreach ($messages as $message)
    	    {
    	        $this->flashMessenger()->addErrorMessage($message);
    	    }
    		$this->redirect()->toRoute('application');
    	}    	
    }
    
    public function logoutAction()
    {
        $authenticator = new AuthenticationService();
        $authenticator->clearIdentity();
        
    	return $this->redirect()->toRoute('application');
    }
    
    
    
    
}
