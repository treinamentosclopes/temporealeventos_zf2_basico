<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Hobbit for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Hobbit\Controller;

use Coiote\Controller\ControllerAbstract;

class TocasController extends ControllerAbstract
{
    protected $formClass = 'Hobbit\Form\Tocas';
    
    protected $modelClass = 'Hobbit\Model\Tocas';
    
    protected $module = 'tocas';
    
    public function getTable()
    {
    	return $this->getServiceLocator()->get('TocasTable');
    }
    
}
