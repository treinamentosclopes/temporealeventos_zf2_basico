<?php
namespace Hobbit\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
#use Zend\Authentication\AuthenticationService;

/**
 * IndexController
 *
 * @author
 *
 * @version
 *
 */
class IndexController extends AbstractActionController
{

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {
        /*$authenticator = new AuthenticationService();
        if(!$authenticator->hasIdentity())
        {
        	return $this->redirect()->toRoute('application');
        }*/
        
        
        // TODO Auto-generated IndexController::indexAction() default action
        $viewModel = new ViewModel();
        return $viewModel;
    }
}