<?php
namespace Hobbit\Form;

use Zend\Form\Form;
use Zend\Form\Element\Text;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Submit;
use Coiote\Form\IdInterface;

class Tocas extends Form implements IdInterface
{

    public function __construct($name = 'tocas')
    {
    	parent::__construct($name);
    	
    	$this->setAttribute('method', 'post');
    	
    	$elementOrFieldset = new Text('nome');
    	$elementOrFieldset->setLabel('Nome:');
    	$elementOrFieldset->setAttribute('autofocus', 'autofocus');
    	$this->add($elementOrFieldset);
    	
    	$elementOrFieldset = new Hidden('id');    	 
    	$this->add($elementOrFieldset);
    	
    	$elementOrFieldset = new Submit('gravar');
    	$elementOrFieldset->setValue('gravar');
    	$this->add($elementOrFieldset);
    	    	
    }
    
    /**
     * $tables = array(
     *      'table1' => object of TableGatewayAbstract
     * )
     * @param integer $id
     * @param array $tables
     */
    public function setId($id, array $tables)
    {
    	if($id == null) return;
    	
    	$tocasTable = $tables['TocasTable'];
    	
    	$tocas = $tocasTable->fetchOne($id);
    	
    	if($tocas != null)
    	{
    		$this->get('nome')->setValue($tocas->nome);
    		$this->get('id')->setValue($id);
    	}
    	
    }
    
    
    
}

?>