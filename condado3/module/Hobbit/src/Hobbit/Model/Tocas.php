<?php
namespace Hobbit\Model;

use Zend\Validator\Digits;
use Zend\Filter\Int;
use Zend\I18n\Filter\Alnum;
use Zend\Validator\StringLength;
use Zend\Filter\StringTrim;
use Coiote\Model\ModelAbstract;

class Tocas extends ModelAbstract
{
    /**
     * 
     * @var integer
     */
    public $id = null;
    
    /**
     * 
     * @var string
     */
    public $nome = null;
    
    public function __construct(array $tables = null)
    {
    	parent::__construct($tables);
    	$this->attributes = array(
    		'id' => array(
    		    'allowEmpty' => TRUE,
    		    'filters'    => array(
    			    new Int()
    		    ),
    		    'validators' => array(
    		    	new Digits()
    		    )
    	    ),
    	    'nome' => array(
    	        'allowEmpty' => FALSE,
    	        'filters'    => array(
    	            new StringTrim(),
    	            new Alnum()
    	        ),
    	        'validators' => array(
    	            new StringLength(array(
    	            	'min' => 3,
    	                'max' => 30
    	            ))
    	        )
    	    ),
    	);
    }
}
?>