<?php
namespace Hobbit\Model;

use Zend\Validator\Digits;
use Zend\Filter\Int;
use Zend\I18n\Filter\Alnum;
use Zend\Validator\StringLength;
use Zend\Filter\StringTrim;
use Coiote\Model\ModelAbstract;
use Coiote\Filter\Corintiano;
use Coiote\Validator\HobbitName;

class Hobbit extends ModelAbstract
{
    /**
     * 
     * @var integer
     */
    public $id = null;
    
    /**
     * 
     * @var string
     */
    public $nome = null;
    
    /**
     * 
     * @var Tocas
     */
    public $tocas = null;
    
    public function __construct(array $tables = null)
    {
    	parent::__construct($tables);
    	$this->attributes = array(
    		'id' => array(
    		    'allowEmpty' => TRUE,
    		    'filters'    => array(
    			    new Int()
    		    ),
    		    'validators' => array(
    		    	new Digits()
    		    )
    	    ),
    	    'nome' => array(
    	        'allowEmpty' => FALSE,
    	        'filters'    => array(
    	            new StringTrim(),
    	            new Corintiano(),    	            
    	        ),
    	        'validators' => array(
    	            new StringLength(array(
    	            	'min' => 3,
    	                'max' => 30
    	            )),
    	            new HobbitName()
    	        )
    	    ),
    	    'id_tocas' => array(
    	        'allowEmpty' => FALSE,
    	        'filters'    => array(
    	            new Int()
    	        ),
    	        'validators' => array(
    	            new Digits()
    	        )
    	    ),
    	);
    }
       
    public function exchangeArray(array $array)
    {
    	$this->id   = isset($array['id']) ? $array['id'] : null;
    	$this->nome = isset($array['nome']) ? $array['nome'] : null;
    	
    	$idTocas = isset($array['id_tocas']) ? $array['id_tocas'] : null;
    	
    	// aqui verificamos se a variavel protegida é um objeto
    	if(!$this->tocas instanceof Tocas)
    	{
    	    $tocasTable = $this->tables['TocasTable'];
    	    $toca = $tocasTable->fetchOne($idTocas);
    	    $this->tocas = $toca;
    	}
    	
    	if($this->tocas == null)
    	{
    	    $this->tocas = new Tocas();
    	}   
    	 	    
    	$this->tocas->id = $idTocas;
    }       
    
    public function getArrayCopy()
    {
        $array = get_object_vars($this);
        $array['id_tocas'] = $this->tocas->id;
        return $array;
    }
}
?>